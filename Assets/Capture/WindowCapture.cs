﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using UnityEngine;
using Graphics = System.Drawing.Graphics;

namespace Assets.Capture
{
    class WindowCapture
    {
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);

        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        public Bitmap Capture(string procName)
        {
            //var processes = System.Diagnostics.Process.GetProcessesByName(procName);
            //var proc = processes[0];

            //var bmp = PrintWindow(proc.MainWindowHandle);

            //RECT rect;
            //HandleRef handleRef = new HandleRef(this, proc.MainWindowHandle);
            //if (GetWindowRect(proc.MainWindowHandle, out rect))
            //{
            //    Debug.Log(rect);
            //}
            //else
            //{
            //    Debug.LogError("Error when using GetWindowRect");
            //}

            //int width = rect.Width;
            //int height = rect.Height;

            int width = 1920;
            int height = 1080;

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                //graphics.CopyFromScreen(rect.Left, rect.Top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
                graphics.CopyFromScreen(2560, 360, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

                //IntPtr hdcBitmap = graphics.GetHdc();

                //PrintWindow(proc.MainWindowHandle, hdcBitmap, 0);

                //graphics.ReleaseHdc(hdcBitmap);
                //graphics.Dispose();
            }

            //bmp.Save(@"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\test.png", ImageFormat.Png);
            return bmp;
        }

        public static Bitmap PrintWindow(IntPtr hwnd)
        {
            RECT rc;
            //GetWindowRect(hwnd, out rc);
            if (GetWindowRect(hwnd, out rc))
            {
                Debug.Log(rc);
            }
            else
            {
                Debug.LogError("Error when using GetWindowRect");
            }

            Bitmap bmp = new Bitmap(500, 500, PixelFormat.Format32bppArgb);
            Graphics gfxBmp = Graphics.FromImage(bmp);
            IntPtr hdcBitmap = gfxBmp.GetHdc();

            PrintWindow(hwnd, hdcBitmap, 0);

            gfxBmp.ReleaseHdc(hdcBitmap);
            gfxBmp.Dispose();

            return bmp;
        }
    }
}
