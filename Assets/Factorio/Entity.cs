﻿
namespace Assets.Factorio
{
    internal class Entity
    {
        public string id;
        public string name;
        public Position position;
        public float orientation;
    }
}