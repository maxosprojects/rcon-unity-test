using Assets.Factorio;
using Newtonsoft.Json;
using Rcon.Client;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using Assets.Capture;
using System;

public class UpdateScript : MonoBehaviour
{
    public GameObject arrowPrefab;

    private RconClient client;
    private Dictionary<string, Sector> sectors = new Dictionary<string, Sector>();
    private FileStream fs = null;
    private string fileName = @"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\updates";
    private StreamWriter debugFileStream = null;
    private string debugFileName = @"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\debug.txt";
    private int REMOVE_FILE_ON_BYTES = 20_000_000;
    private string lastString = "";
    public float zoomSpeed = 1;
    public float targetOrtho;
    public float smoothSpeed = 20.0f;
    public float minOrtho = 1.0f;
    public float maxOrtho = 20.0f;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("starting");
        targetOrtho = Camera.main.orthographicSize;
        //client = new RconClient("localhost", 25575);
        //client = new RconClient("192.168.0.34", 25575);
        //client.Connection.Open();
        //var res = client.ExecuteCommand(RconCommand.Auth("factory"));
        //Debug.Log("authenticated");

        //new WindowCapture().Capture("Factorio");

        debugFileStream = new StreamWriter(debugFileName);
    }

    private void OnDestroy()
    {
        if (fs != null)
        {
            fs.Close();
            fs = null;
        }
        if (debugFileStream != null)
        {
            debugFileStream.Close();
            debugFileStream = null;
        }
        Debug.Log("Destroyed");
    }

    // Update is called once per frame
    void Update()
    {
        updateViaFileStream();
        updateCamera();
    }

    private void updateCamera()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            targetOrtho -= scroll * zoomSpeed;
            targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho);
        }

        Camera.main.orthographicSize = Mathf.MoveTowards(Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
    }

    private void updateViaFileStream()
    {
        if (fs == null)
        {
            if (!File.Exists(fileName))
            {
                return;
            }
            fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }
        var position = fs.Position;
        //var end = fs.Seek(0, SeekOrigin.End);
        var end = fs.Length;
        if (position == end)
        {
            return;
        }
        //fs.Seek(position, SeekOrigin.Begin);

        var count = end - position;
        var bytes = new byte[count];
        fs.Read(bytes, 0, (int)count);
        var strData = lastString + Encoding.Default.GetString(bytes);
        var lines = strData.Split('\n');
        // At this point there is at least one element in 'lines' array
        lastString = lines[lines.Length - 1];
        if (lines.Length < 2) return;
        foreach(var line in lines)
        {
            if ("".Equals(line))
            {
                continue;
            }
            try
            {
                updateEntities(line);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);

                debugFileStream.WriteLine(ex);
                debugFileStream.WriteLine(line);
                debugFileStream.WriteLine("\n");
                debugFileStream.Flush();
            }
        }
        if (fs.Length > REMOVE_FILE_ON_BYTES)
        {
            fs.Close();
            fs = null;
            File.Delete(fileName);
        }
    }

    private void updateEntities(string updateString)
    {
        updateString = updateString.Replace("\"entities\":{}", "\"entities\":[]");
        UpdateRecord updateRecord = JsonConvert.DeserializeObject<UpdateRecord>(updateString);
        var sectorId = updateRecord.sector;
        var playerPos = updateRecord.player.pos;
        var idsToStay = new HashSet<string>();
        Sector sector;
        if (sectors.ContainsKey(sectorId))
        {
            sector = sectors[sectorId];
        }
        else
        {
            sector = new Sector();
            sectors.Add(sectorId, sector);
        }
        var arrows = sector.arrows;
        foreach (var entity in updateRecord.entities)
        {
            if (arrows.ContainsKey(entity.id))
            {
                var arrow = arrows[entity.id];
                arrow.transform.position = getEntityPosition(playerPos, entity.position);
                arrow.transform.rotation = getAngle(entity.orientation);
            }
            else
            {
                var arrow = Instantiate(arrowPrefab, getEntityPosition(playerPos, entity.position), getAngle(entity.orientation));
                arrows.Add(entity.id, arrow);
            }
            idsToStay.Add(entity.id);
        }
        var idsToRemove = new HashSet<string>(arrows.Keys);
        idsToRemove.ExceptWith(idsToStay);
        foreach (var id in idsToRemove)
        {
            Destroy(arrows[id]);
            arrows.Remove(id);
        }
    }

    private Quaternion getAngle(float orientation)
    {
        return Quaternion.Euler(0, 0, -orientation * 360);
    }

    private Vector3 getEntityPosition(Position playerPos, Position entityPos)
    {
        return new Vector3((entityPos.x - playerPos.x) / 8, (playerPos.y - entityPos.y) / 8);
    }
}
