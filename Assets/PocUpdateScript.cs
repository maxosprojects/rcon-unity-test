using Assets.Factorio;
using Newtonsoft.Json;
using Rcon.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class PocUpdateScript : MonoBehaviour
{
    public GameObject arrowPrefab;

    private RconClient client;
    private Dictionary<string, GameObject> arrows = new Dictionary<string, GameObject>();
    private DateTime startedAt;
    private FileStream fs;
    private string fileName = @"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\enemies";
    private int updatesNum = 0;
    private int lastTick = 0;
    //private StreamReader sr;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("starting");
        client = new RconClient("localhost", 25575);
        client.Connection.Open();
        var res = client.ExecuteCommand(RconCommand.Auth("factory"));
        Debug.Log("authenticated");
        startedAt = DateTime.Now;
        //sr = new StreamReader(fs);
    }

    // Update is called once per frame
    void Update()
    {
        //updateViaRcon();
        //updateViaFileMove();
        //ticksFromFileStream();
        updateViaFileStream();
    }

    private void updateViaFileStream()
    {
        if (fs == null)
        {
            if (!File.Exists(fileName))
            {
                return;
            }
            fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }
        var position = fs.Position;
        var end = fs.Seek(0, SeekOrigin.End);
        if (position == end)
        {
            return;
        }
        fs.Seek(position, SeekOrigin.Begin);
        //var lines = sr.ReadToEnd().Split('\n');
        var count = end - position;
        var bytes = new byte[count];
        fs.Read(bytes, 0, (int)count);
        var lines = Encoding.Default.GetString(bytes).Split('\n');
        if (lines.Length < 2 || "".Equals(lines[lines.Length - 2]))
        {
            return;
        }
        updateEnemies(lines[lines.Length - 2]);
        if (fs.Length > 5_000_000)
        {
            fs.Close();
            fs = null;
            File.Delete(fileName);
        }
    }

    private void updateEnemies(string enemies)
    {
        List<Entity> units = JsonConvert.DeserializeObject<List<Entity>>(enemies);
        foreach (var unit in units)
        {
            if (!arrows.ContainsKey(unit.id))
            {
                var arrow = Instantiate(arrowPrefab, getPosition(unit.position), Quaternion.identity);
                arrows.Add(unit.id, arrow);
            }
            else
            {
                var arrow = arrows[unit.id];
                arrow.transform.position = getPosition(unit.position);
                arrow.transform.rotation = Quaternion.Euler(0, 0, -unit.orientation * 360);
            }
        }
    }

    private Vector3 getPosition(Position pos)
    {
        return new Vector3(pos.x, pos.y);
    }

    private void ticksFromFileStream()
    {
        var position = fs.Position;
        var end = fs.Seek(0, SeekOrigin.End);
        if (position == end)
        {
            return;
        }
        fs.Seek(position, SeekOrigin.Begin);
        //var lines = sr.ReadToEnd().Split('\n');
        var count = end - position;
        var bytes = new byte[count];
        fs.Read(bytes, 0, (int)count);
        var lines = Encoding.Default.GetString(bytes).Split('\n');
        checkLines(lines);
    }

    private void checkLines(string[] lines)
    {
        foreach (var line in lines)
        {
            if ("".Equals(line))
            {
                return;
            }
            //Debug.Log(line);
            var tick = Int32.Parse(line);
            //Debug.Log(tick);
            if (lastTick == 0)
            {
                lastTick = tick;
            }
            else
            {
                if (lastTick + 1 != tick)
                {
                    Debug.Log("Previous tick was " + lastTick + "and next is " + tick);
                }
                lastTick = tick;
            }
        }
    }

    private void updateViaFileMove()
    {
        var file1 = @"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\file1";
        var file2 = @"C:\Users\Maksym\AppData\Roaming\Factorio\script-output\file2";
        try
        {
            File.Move(file1, file2);
            var lines = File.ReadAllLines(file2);
            checkLines(lines);
            File.Delete(file2);
        }
        catch
        {

        }
    }

    private void updateViaRcon()
    {
        var playerCmdStartedAt = DateTime.Now;
        var playerCmd = @"/sc local pos = game.get_player(1).position; rcon.print(game.table_to_json(pos))";
        var res = client.ExecuteCommand(RconCommand.ServerCommand(playerCmd));
        Debug.Log("PlayerCmd took: " + (DateTime.Now - playerCmdStartedAt).TotalMilliseconds);
        Debug.Log(res.ResponseText);

        var playerPos = JsonConvert.DeserializeObject<Position>(res.ResponseText);
        //var enemiesCmd = "/time";
        var enemiesCmdStartedAt = DateTime.Now;
        var enemiesCmd = @"/sc local res = {};" +
            @"for key, entity in pairs(game.get_surface('nauvis').find_enemy_units(game.get_player(1).position, 80)) do" +
            @"  table.insert(res, { id=entity.unit_number, position={ x=string.format('%.3f', entity.position['x']), y=string.format('%.3f', entity.position['y']) }, orientation=string.format('%.3f', entity.orientation) })" +
            @"end;" +
            @"rcon.print(game.table_to_json(res));";
        res = client.ExecuteCommand(RconCommand.ServerCommand(enemiesCmd));
        Debug.Log("EnemiesCmd took: " + (DateTime.Now - enemiesCmdStartedAt).TotalMilliseconds);
        Debug.Log(res.ResponseText);

        updatesNum++;

        List<Entity> units = JsonConvert.DeserializeObject<List<Entity>>(res.ResponseText);
        foreach (var unit in units)
        {
            if (!arrows.ContainsKey(unit.id))
            {
                var arrow = Instantiate(arrowPrefab, getPosition(playerPos, unit), Quaternion.identity);
                arrows.Add(unit.id, arrow);
            }
            else
            {
                var arrow = arrows[unit.id];
                arrow.transform.position = getPosition(playerPos, unit);
                arrow.transform.rotation = Quaternion.Euler(0, 0, -unit.orientation * 360);
            }
        }

        var timeElapsed = DateTime.Now.Subtract(startedAt);
        Debug.Log("Updates/s: " + (updatesNum / timeElapsed.TotalSeconds));
    }

    private Vector3 getPosition(Position playerPos, Entity unit)
    {
        return new Vector3((unit.position.x - playerPos.x) / 8, (playerPos.y - unit.position.y) / 8);
    }
}
